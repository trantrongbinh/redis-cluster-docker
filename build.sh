#!/bin/sh

if [ "$1" = 'up' ]; then
  docker-compose up --detach --scale redis-master=1 --scale redis-secondary=3
elif [ "$1" = 'down' ]; then
  docker-compose down
else
  echo "Using up or down option!"
fi
