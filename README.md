# docker-redis-cluster

- [Doc](https://github.com/bitnami/bitnami-docker-redis)

- Sample config:
```
tcp-backlog 511
timeout 0
tcp-keepalive 300
dbfilename dump.rdb
dir /var/lib/redis
maxclients 20000
maxmemory 10G
maxmemory-policy allkeys-lru
appendonly no
```

- cd into repo
- Build basic:
```
docker-compose up --build -d
```
- Scale the number of replicas using:
```
docker-compose up --detach --scale redis-master=1 --scale redis-secondary=3
```
The above command scales up the number of replicas to 3. You can scale down in the same way.

=> Note: You should not scale up/down the number of master nodes. Always have only one master node running.

# install

- clone repo
- config kernel
```bash
sysctl vm.overcommit_memory=1
```
- or edit file: /etc/sysctl.conf and add line:

```
vm.overcommit_memory=1
net.core.somaxconn=65535
```
- then run:
```
sudo sysctl --system

echo never > /sys/kernel/mm/transparent_hugepage/enabled
```
- final run:
```
. build.sh up
```
