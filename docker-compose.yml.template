version: '3'

services:
  redis-primary:
    image: docker.io/bitnami/redis:6.2
    ports:
      - '7001:7001'
    environment:
      - REDIS_PORT_NUMBER=7001
      - REDIS_REPLICA_PORT=7001
      - REDIS_REPLICATION_MODE=master
      - REDIS_PASSWORD=my_password
      - REDIS_DISABLE_COMMANDS=FLUSHDB,FLUSHALL
      - REDIS_AOF_ENABLED=no
      - REDIS_EXTRA_FLAGS=--maxmemory 100mb
    sysctls:
      net.core.somaxconn: 1024
    volumes:
      - 'redis_data:/bitnami/redis/data'

  redis-secondary:
    image: docker.io/bitnami/redis:6.2
    ports:
      - '7001'
    depends_on:
      - redis-primary
    environment:
      - REDIS_REPLICATION_MODE=slave
      - REDIS_MASTER_HOST=redis-primary
      - REDIS_MASTER_PORT_NUMBER=7001
      - REDIS_MASTER_PASSWORD=my_password
      - REDIS_PASSWORD=my_password
      - REDIS_DISABLE_COMMANDS=FLUSHDB,FLUSHALL
      - REDIS_AOF_ENABLED=no
      - REDIS_EXTRA_FLAGS=--maxmemory 100mb
    sysctls:
      net.core.somaxconn: 1024

volumes:
  redis_data:
    driver: local
